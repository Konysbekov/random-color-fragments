package com.example.randomcolorfragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import kotlin.random.Random

class FragmentOne : Fragment() {

    private var colorFragmentTwo: Int = Color.WHITE
    private var colorFragmentThree: Int = Color.WHITE
    private var isFragmentTwoActive: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_one, container, false)

        savedInstanceState?.let {
            colorFragmentTwo = it.getInt("colorFragmentTwo", Color.WHITE)
            colorFragmentThree = it.getInt("colorFragmentThree", Color.WHITE)
            isFragmentTwoActive = it.getBoolean("isFragmentTwoActive", true)
        }

        val buttonChangeColor = view.findViewById<Button>(R.id.buttonChangeColor)
        val buttonSwapFragments = view.findViewById<Button>(R.id.buttonSwapFragments)

        buttonChangeColor.setOnClickListener {
            changeColors()
        }

        buttonSwapFragments.setOnClickListener {
            swapFragments()
        }

        if (savedInstanceState == null) {
            if (isFragmentTwoActive) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container_view_tag2, FragmentTwo.newInstance(colorFragmentTwo))
                    .replace(R.id.fragment_container_view_tag3, FragmentThree.newInstance(colorFragmentThree))
                    .commit()
            } else {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container_view_tag2, FragmentThree.newInstance(colorFragmentThree))
                    .replace(R.id.fragment_container_view_tag3, FragmentTwo.newInstance(colorFragmentTwo))
                    .commit()
            }
        }

        return view
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("colorFragmentTwo", colorFragmentTwo)
        outState.putInt("colorFragmentThree", colorFragmentThree)
        outState.putBoolean("isFragmentTwoActive", isFragmentTwoActive)
    }

    private fun changeColors() {
        colorFragmentTwo = generateRandomColor()
        colorFragmentThree = generateRandomColor()

        updateColors()
    }

    private fun swapFragments() {
        isFragmentTwoActive = !isFragmentTwoActive

        if (isFragmentTwoActive) {
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container_view_tag2, FragmentTwo.newInstance(colorFragmentTwo))
                .replace(R.id.fragment_container_view_tag3, FragmentThree.newInstance(colorFragmentThree))
                .commit()
        } else {
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container_view_tag2, FragmentThree.newInstance(colorFragmentThree))
                .replace(R.id.fragment_container_view_tag3, FragmentTwo.newInstance(colorFragmentTwo))
                .commit()
        }
    }

    private fun updateColors() {
        val fragmentTwo = parentFragmentManager.findFragmentById(R.id.fragment_container_view_tag2)
        val fragmentThree = parentFragmentManager.findFragmentById(R.id.fragment_container_view_tag3)

        fragmentTwo?.view?.setBackgroundColor(colorFragmentTwo)
        fragmentThree?.view?.setBackgroundColor(colorFragmentThree)
    }

    private fun generateRandomColor(): Int {
        val random = Random
        return Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256))
    }
}
