package com.example.randomcolorfragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.os.bundleOf

class FragmentTwo : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val color = arguments?.getInt(BACKGROUND_COLOR)
        val rootView = view.findViewById<FrameLayout>(R.id.root)
        rootView.setBackgroundColor(color ?: 0)
    }

    companion object {
        const val BACKGROUND_COLOR = "BACKGROUND_COLOR"

        fun newInstance(color: Int): FragmentTwo {
            val fragment = FragmentTwo()
            fragment.arguments = bundleOf(BACKGROUND_COLOR to color)
            return fragment
        }
    }
}
